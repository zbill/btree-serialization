/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dongbat.tree.minhBehavior;

import com.badlogic.gdx.ai.btree.LeafTask;
import com.badlogic.gdx.ai.btree.Task;

/**
 *
 * @author Admin
 */
public class MarkTask extends LeafTask<Dog> {

  private int i = 0;

  @Override
  public void run(Dog dog) {
    i++;
    if (i % 2 == 0) {
      dog.markATree();
      dog.setUrgent(false);
      success();
    } else {
      this.fail();
    }
  }

  @Override
  protected Task<Dog> copyTo(Task<Dog> task) {
    return task;
  }

}
