/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dongbat.tree.minhBehavior;

import com.badlogic.gdx.ai.btree.LeafTask;
import com.badlogic.gdx.ai.btree.Task;

/**
 *
 * @author Admin
 */
public class WalkTask extends LeafTask<Dog> {

  private int i = 0;

  @Override
  public void start(Dog dog) {
    i = 0;
    dog.startWalking();
  }

  @Override
  public void run(Dog dog) {
    i++;
    dog.randomlyWalk();
    if (i < 3) {
      running();
    } else {
      success();
    }
  }

  @Override
  public void end(Dog dog) {
    dog.stopWalking();
  }

  @Override
  protected Task<Dog> copyTo(Task<Dog> task) {
    return task;
  }

}
