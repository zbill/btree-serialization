/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dongbat.tree.minhBehavior;

import com.badlogic.gdx.ai.btree.LeafTask;
import com.badlogic.gdx.ai.btree.Task;

/**
 *
 * @author Admin
 */
public class RestTask extends LeafTask<Dog> {

  @Override
  public void run(Dog dog) {
    if (dog.isUrgent()) {
      dog.brainLog("No time to rest");
      fail();
    } else {
      dog.brainLog("zz zz");
      running();
    }

  }

  @Override
  protected Task<Dog> copyTo(Task<Dog> task) {
    return task;
  }

}
