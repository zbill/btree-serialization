/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dongbat.tree.minhBehavior;

import com.badlogic.gdx.Gdx;

/**
 *
 * @author Admin
 */
public class Dog {

  private String name;
  private String brainLog;
  private String definitionPath;

  public Dog() {
  }

  public Dog(String name) {
    this.name = name;
    this.brainLog = name + " brain";
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBrainLog() {
    return brainLog;
  }

  public void setBrainLog(String brainLog) {
    this.brainLog = brainLog;
  }

  public String getDefinitionPath() {
    return definitionPath;
  }

  public void setDefinitionPath(String definitionPath) {
    this.definitionPath = definitionPath;
  }

  public void bark() {
    log("Bow wow!!!");
  }

  public void startWalking() {
    log("Dog starts walking");
  }

  public void randomlyWalk() {
    log("Dog walks randomly around!");
  }

  public void stopWalking() {
    log("Dog stops walking");
  }

  public boolean standBesideATree() {
    if (Math.random() < 0.5) {
      log("No tree found :(");
      return false;
    }
    return true;
  }

  public void markATree() {
    log("Dog lifts a leg and pee!");
  }

  private boolean urgent = false;

  public boolean isUrgent() {
    return urgent;
  }

  public void setUrgent(boolean urgent) {
    this.urgent = urgent;
  }

  public void log(String msg) {
    Gdx.app.log(name, msg);
  }

  public void brainLog(String msg) {
    Gdx.app.log(brainLog, msg);
  }
}
