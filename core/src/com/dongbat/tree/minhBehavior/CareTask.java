/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dongbat.tree.minhBehavior;

import com.badlogic.gdx.ai.btree.LeafTask;
import com.badlogic.gdx.ai.btree.Metadata;
import com.badlogic.gdx.ai.btree.Task;

/**
 *
 * @author Admin
 */
public class CareTask extends LeafTask<Dog> {

  public static final Metadata METADATA = new Metadata("urgentProb");

  public float urgentProb = 0.8f;
  private int i = 0;

  @Override
  public void run(Dog dog) {
    i++;
    if (i % 5 != 0) {
      success();
    } else {
      dog.brainLog("It's leaking out!!!");
      dog.setUrgent(true);
      success();
    }
  }

  @Override
  protected Task<Dog> copyTo(Task<Dog> task) {
    CareTask care = (CareTask) task;
    care.urgentProb = urgentProb;

    return task;
  }

}
