/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dongbat.tree;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.ai.btree.BranchTask;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.ai.btree.branch.Parallel;
import com.badlogic.gdx.ai.btree.utils.BehaviorTreeParser;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Field;
import com.badlogic.gdx.utils.reflect.ReflectionException;
import com.dongbat.tree.minhBehavior.Dog;
import com.dongbat.tree.utils.KryoTree;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class GameScene implements Screen {

  private Stage stage;
  private TextureRegion region;
  private ShapeRenderer renderer;
  private Skin skin;
  private TextButton stepButton;
  private TextButton saveButton;
  private TextButton loadButton;
  private BehaviorTree<Dog> tree;
  private final Tree displayTree;
  private final Dog dog;

  public void step() {
    System.out.println(tree.getObject());
    tree.step();
  }

  public void save() {
    KryoTree.saveTree(tree);
  }

  public void load() {
    tree = KryoTree.loadTree();
    System.out.println(tree.getObject());
    tree.setObject(dog);
    redrawTree();
  }

  public GameScene() {
    BehaviorTreeParser<Dog> parser = new BehaviorTreeParser<Dog>(BehaviorTreeParser.DEBUG_NONE);
    dog = new Dog("Dog 1");
    tree = parser.parse(Gdx.files.internal("dog.tree"), dog);
    KryoTree.initKryo();
    renderer = new ShapeRenderer();
    skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
    region = new TextureRegion(new Texture("badlogic.jpg"));

    stepButton = new TextButton("step", skin);

    loadButton = new TextButton("load", skin);

    saveButton = new TextButton("save", skin);

    stage = new Stage(new ScreenViewport());

    Table table = new Table();
    table.row().height(20).fillX();
    table.add(stepButton);
    table.add(saveButton);
    table.add(loadButton);
    table.row();
    displayTree = new Tree(skin);

    redrawTree();

    table.add(displayTree).colspan(3).fillX().fillY().expand(true, true);

    stage.addActor(table);
    table.setFillParent(true);

    saveButton.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        save();
      }
    });

    loadButton.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        load();
      }
    });

    stepButton.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        step();
      }
    });

    Gdx.input.setInputProcessor(stage);
  }

  @Override
  public void show() {
  }

  @Override
  public void render(float delta) {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    stage.act(delta);
    stage.draw();
  }

  @Override
  public void resize(int width, int height) {
    stage.getViewport().update(width, height, true);
  }

  @Override
  public void pause() {
  }

  @Override
  public void resume() {
  }

  @Override
  public void hide() {
  }

  @Override
  public void dispose() {
    stage.dispose();
  }

  private void redrawTree() {
    displayTree.clear();
    Task<Dog> root = tree.getChild(0);
    addToTree(displayTree, null, root);
    displayTree.expandAll();
  }

  private static class MyLabel extends Label {

    private String content;
    public MyNode node;
    public boolean running = false;

    public MyLabel(String content, Skin skin) {
      super(content, skin);
      this.content = content;
    }

    @Override
    public void act(float delta) {
      if (running) {
        setText(content + " (running)");
      } else {
        setText(content);
      }

      if (node.task instanceof Parallel) {
        Array<Task> runningTasks = getAttribute(node.task, "runningTasks", Array.class, Parallel.class);
        Array<Tree.Node> children = node.getChildren();
        for (Tree.Node c : children) {
          MyNode node = (MyNode) c;
          node.label.running = false;
          if (runningTasks.contains(node.task, true)) {
            node.label.running = true;
          }
        }
      } else if (node.task instanceof BranchTask) {

        Task runningTask = getAttribute(node.task, "runningTask", Task.class, Task.class);
        Array<Tree.Node> children = node.getChildren();
        for (Tree.Node c : children) {
          MyNode node = (MyNode) c;
          node.label.running = false;
          if (node.task == runningTask) {
            node.label.running = true;
          }
        }
      }
    }

  }

  private static class MyNode extends Tree.Node {

    public MyLabel label;
    public Task task;

    public MyNode(MyLabel label, Task task) {
      super(label);
      this.label = label;
      this.task = task;
      label.node = this;
    }

  }

  private void addToTree(Tree displayTree, MyNode parentNode, Task<Dog> task) {
    MyNode node = new MyNode(new MyLabel(task.getClass().getSimpleName(), skin), task);
    if (parentNode == null) {
      displayTree.add(node);
    } else {
      parentNode.add(node);
    }
    for (int i = 0; i < task.getChildCount(); i++) {
      Task<Dog> child = task.getChild(i);
      addToTree(displayTree, node, child);
    }
  }

  public static <T> T getAttribute(Object obj, String name, Class<T> type, Class clazz) {
    try {
      Field field = ClassReflection.getDeclaredField(clazz, name);
      field.setAccessible(true);
      Object attr = field.get(obj);
      return (T) attr;
    } catch (ReflectionException ex) {
      Logger.getLogger(GameScene.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
  }

}
