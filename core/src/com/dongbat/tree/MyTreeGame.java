package com.dongbat.tree;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MyTreeGame extends Game {

  SpriteBatch batch;
  Texture img;

  @Override
  public void create() {
    setScreen(new GameScene());
  }

}
