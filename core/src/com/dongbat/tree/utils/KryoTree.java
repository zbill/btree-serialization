/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dongbat.tree.utils;

import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.dongbat.tree.minhBehavior.Dog;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.ByteBufferInput;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.OutputChunked;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import org.objenesis.strategy.StdInstantiatorStrategy;

/**
 *
 * @author Admin
 */
public class KryoTree {

  private static Kryo kryo;
  private static BehaviorTree<Dog> copy;
  private static OutputChunked output = new OutputChunked();

  public KryoTree() {
  }

  public static void initKryo() {
    kryo = new Kryo();
    kryo.setInstantiatorStrategy(new StdInstantiatorStrategy());
    kryo.register(Dog.class);
    FieldSerializer fieldSerializer = new FieldSerializer(kryo, BehaviorTree.class);
    fieldSerializer.removeField("object");
    kryo.register(BehaviorTree.class, fieldSerializer);
  }

  public static void saveTree(BehaviorTree tree) {
    output.clear();
    kryo.writeObjectOrNull(output, tree, tree.getClass());
    System.out.println(output.total());
  }

  public static BehaviorTree loadTree() {
    Input input = new ByteBufferInput(output.getBuffer());
    return kryo.readObjectOrNull(input, BehaviorTree.class);
  }
}
